package org.netty.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
 
public class PropertyUtils {
    private static final Logger logger = Logger.getLogger(PropertyUtils.class);
 
    private static final Properties properties;
    public static final byte header=0x58;
    public static final byte footer=0x63;

    static {
        properties = new Properties();
        InputStream inputStream = PropertyUtils.class.getClassLoader().getResourceAsStream("config.properties");
        if (inputStream != null) {
            try {
                properties.load(inputStream);
            } catch (IOException e) {
                logger.error("读取配置文件失败", e);
            }
        }
    }
 
    //供外部调用，配置文件只读取一次，节约内存
    public static String getProperty(String param) {
        return properties.getProperty(param);
    }
    public static Integer getIntegerValue(String param) {
        return Integer.valueOf(properties.getProperty(param));
    }
}