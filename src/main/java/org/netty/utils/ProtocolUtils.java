package org.netty.utils;

import org.netty.protocol.Protocol;
import org.netty.utils.serial.SerializeDeserializeWrapper;
import org.netty.utils.serial.SerializeUtils;

public class ProtocolUtils {
    /***
     *
     * 构建对象
     */
    public static Protocol prtclInstance(Object o){
        Protocol protocol = new Protocol();
        byte [] objectBytes= SerializeUtils.serialize(SerializeDeserializeWrapper.builder(o));
        protocol.setLen(objectBytes.length);
        protocol.setData(objectBytes);
        return protocol;
    }

}
