package org.netty.protocol;

import org.netty.utils.PropertyUtils;

import java.util.Arrays;

public class Protocol {
    //帧头
    private byte header;
    //数据（data）长度
    private int len;
    //数据
    private byte [] data;
    //帧尾
    private byte tail;

    public byte getTail() {
        return PropertyUtils.footer;
    }

    public void setTail(byte tail) {
        this.tail = tail;
    }

    public Protocol(int len, byte[] data) {
        this.len = len;
        this.data = data;
    }

    public Protocol() {
    }

    @Override
    public String toString() {
        return "Protocol{" +
                "header=" + header +
                ", len=" + len +
                ", data=" + Arrays.toString(data) +
                ", tail=" + tail +
                '}';
    }

    public byte getHeader() {
        return PropertyUtils.header;
    }

    public void setHeader(byte header) {
        this.header = header;
    }

    public int getLen() {
        return len;
    }

    public void setLen(int len) {
        this.len = len;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

}
